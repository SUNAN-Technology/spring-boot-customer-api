FROM openjdk:8-jdk-alpine
ADD target/*.jar customer-api.jar
ENTRYPOINT ["java", "-jar", "customer-api.jar"]