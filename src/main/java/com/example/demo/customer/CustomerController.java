package com.example.demo.customer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 


@RestController
@RequestMapping("/customer")
public class CustomerController {

	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

	 @Autowired
	 CustomerRepository customerRepository;

	@GetMapping()
	public ResponseEntity<?> getAllList() {
		logger.info("Controller: Fetching list customer details");
		return new ResponseEntity<>(customerRepository.findAll(),
				HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getById(@PathVariable int id,@RequestHeader("customerId") int customerId) {
		logger.info("Controller: Fetching customer details with id {}", id);
		return new ResponseEntity<>(customerRepository.findById(customerId), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<?> save(@RequestBody Customer customer) {
		logger.info("Controller: Save customer details ");
		return new ResponseEntity<>(customerRepository.save(customer), HttpStatus.OK);
	}
}
