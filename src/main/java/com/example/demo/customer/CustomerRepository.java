package com.example.demo.customer;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer> {

	public Optional<Customer> findById(int id);
	
}
